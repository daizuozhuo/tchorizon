/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is the Image react component implementation.
 *
 * @providesModule Image
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var React = require('react');

var ContainerMixin = require('./ContainerMixin');
var QuadMixin = require('./QuadMixin');
var LayerMixin = require('./LayerMixin');
var createComponent = require('./createComponent');
var DrawingUtils = require('./DrawingUtils');

/**
 * This function is used to draw the image component.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {number} translatedX The translated x position
 * @param {number} translatedY The translated y position
 */
function _drawImageRenderLayer (ctx, translatedX, translatedY) {

	var texture = DrawingUtils.getImageTexture(ctx, this.imageUrl);
	if (null != texture) {
    	DrawingUtils.drawTexture(ctx, translatedX, translatedY, this.width, 
    		this.height, texture);
	}
}

// Declare the Image component
var Image = createComponent('Image', LayerMixin, ContainerMixin, QuadMixin, {
    /**
     * Represents the property types.
     */
	propTypes: {
	    src: React.PropTypes.string.isRequired
	},

  	/**
     * This method is called to apply the component properties.
     * @param prevProps the previous properties
     * @param props the new properties
     */
    applyComponentProps: function (prevProps, props) {
        this.applyLayerProps(prevProps, props);
        this.node.imageUrl = props.src;
        this.node.drawRenderLayer = _drawImageRenderLayer;
    }

});

module.exports = Image;