/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This module provides utility method to create a ReactWebGL component.
 *
 * Adapted from react-canvas (https://github.com/Flipboard/react-canvas).
 *
 * @providesModule createComponent
 * @version 1.0
 * @author react-canvas(https://github.com/Flipboard/react-canvas), albertwang, TCSASSEMBLER
 */
'use strict';

var assign = require('react/lib/Object.assign');
var RenderLayer = require('./RenderLayer');

/**
 * Create a ReactWebGL component.
 * @param name the name of the component
 * @returns {Function} the ReactWebGL component
 */
function createComponent(name) {
    var ReactWebGLComponent = function (props) {
        this.node = null;
        this.node = new RenderLayer();
        this._mountImage = null;
        this._renderedChildren = null;
        this._mostRecentlyPlacedChild = null;
    };
    ReactWebGLComponent.displayName = name;
    // try to union the property types and the getDefaultProps results together in case more than
    // one argument contain the propTypes property or the getDefaultProps function
    var propTypes = null;
    var defaultProps = null;
    for (var i = 1, l = arguments.length; i < l; i++) {
        if (arguments[i]['propTypes'] != undefined && typeof arguments[i]['propTypes'] === 'object') {
            if (null == propTypes) {
                propTypes = arguments[i]['propTypes'];
            } else {
                assign(propTypes, arguments[i]['propTypes']);
            }
        }
        if (arguments[i]['getDefaultProps'] != undefined && typeof arguments[i]['getDefaultProps'] === 'function') {
            if (null == defaultProps) {
                defaultProps = arguments[i].getDefaultProps();
            } else {
                assign(defaultProps, arguments[i].getDefaultProps());
            }
        }
        assign(ReactWebGLComponent.prototype, arguments[i]);
    }
    if (null != propTypes) {
        ReactWebGLComponent.prototype.propTypes = propTypes;
    }
    if (null != defaultProps) {
        ReactWebGLComponent.prototype.getDefaultProps = function () {
            return defaultProps;
        }
    }
    return ReactWebGLComponent;
}

module.exports = createComponent;