/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This class defines the row which composes with cells in the application.
 *
 * @providesModule Row
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var Cell = require('./Cell');

/**
 * Constructor for Row
 */
function Row (options) {
	this._cells = [];
	this._initX = options.x;
	this._initY = options.y;
	this._offsetX = 0;
	this._offsetY = 0;
	this._height = options.rowHeight;
	this._width = options.cellWidth * options.items.length;
	this._cellWidth = options.cellWidth;
	this._selected = options.y === 0 ? true : false;

	var i, items = options.items;
	for (i = 0; i < items.length; i += 1) {
        var cell = new Cell({
        	key: 'cell' + i + '_' + options.y, 
        	width: options.cellWidth,
    		height: options.rowHeight, 
    		x: options.x + i * options.cellWidth, 
    		y: options.y,
    		posterBgColor: options.y == 0 && i == 2 ? "rgba(255, 0, 0, 1)" : "rgba(179, 179, 179, 0.5)",
    		item: items[i]});
        this._cells.push(cell);
    }
}

/**
 * Move this section to the specified offset
 * @params offsetX the offset x
 * @params offsetY the offset y
 */
Row.prototype.moveTo = function (offsetX, offsetY) {
	var x, self = this, y = this._initY + offsetY;

	this._cells.forEach(function (cell, index) {
		x = (self._initX + index * self._cellWidth + offsetX) % self._width;
		x = x < 0 ? x + self._width : x;
		cell.setPosition(x, y);

		if (self._selected) {
			if (cell.getX() == 2 * self._cellWidth) {
				cell.setSelected(true);
			} else {
				cell.setSelected(false);
			}
		}
	});

	this._offsetX = offsetX;
	this._offsetY = offsetY;
}

/**
 * Select/unselect this row
 * @params selected whether to select
 */
Row.prototype.setSelected = function(selected) {
	var cellWidth = this._cellWidth;

	this._cells.forEach(function (cell) {
		if (selected && cell.getX() == 2 * cellWidth) {
			cell.setSelected(true);
		} else {
			cell.setSelected(false);
		}
	});
	this._selected = selected;
}

/**
 * Namesake instance variable getter
 * @return the instance variable
 */
Row.prototype.getCells = function () {
	return this._cells;
}

/**
 * Namesake instance variable getter
 * @return the instance variable
 */
Row.prototype.getOffsetX = function () {
	return this._offsetX;
}

/**
 * Namesake instance variable getter
 * @return the instance variable
 */
Row.prototype.getOffsetY = function () {
	return this._offsetY;
}

module.exports = Row;