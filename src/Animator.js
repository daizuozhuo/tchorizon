/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is the animator implementation.
 *
 * @providesModule Animator
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var Tween = require('./Tween');

/**
 * Constructor for Animator
 */
function Animator () {
	this._tweens = [];
};

/**
 * This method is used to start animation for the specitied object.
 * @param targetObject the target to animation
 * @param properties the end properties
 * @param duration the animation duration
 * @param oncomplete the animation complete callback
 * @param onupdate the aniamtion update callback
 */
Animator.prototype.animate = function (targetObject, properties, duration, oncomplete, onupdate) {
	var tween = new Tween(targetObject, properties, duration);
	tween.onComplete(oncomplete);
	tween.onUpdate(onupdate);
	tween.start();

	this._tweens.push(tween);
};

/**
 * Update all active animations
 */
Animator.prototype.update = function () {
	var i, time;
	time = new Date();
	for (i = 0; i < this._tweens.length; ++i) {
		if (this._tweens[i].update(time)) {
			this._tweens.splice(i, 1);
			--i;
		}
	}
};

module.exports = Animator;