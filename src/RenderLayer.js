/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * RenderLayer is used to abstract a canvas layer to render/draw.
 *
 * Adapted from react-canvas (https://github.com/Flipboard/react-canvas).
 *
 * @providesModule RenderLayer
 * @version 1.0
 * @author react-canvas(https://github.com/Flipboard/react-canvas), albertwang, TCSASSEMBLER
 */
'use strict';

var FrameUtils = require('./FrameUtils');

/**
 * Constructor.
 * @constructor
 */
function RenderLayer() {
    this.children = [];
    this.frame = FrameUtils.zero();
}

// Declares the RenderLayer prototype
RenderLayer.prototype = {
    /**
     * Retrieve the root injection layer.
     *
     * @return {RenderLayer} the root layer
     */
    getRootLayer: function () {
        var root = this;
        while (root.parentLayer) {
            root = root.parentLayer;
        }
        return root;
    },

    /**
     * RenderLayers are injected into a root owner layer whenever a WebGL component is
     * mounted. This is the integration point with React internals.
     *
     * @param {RenderLayer} parentLayer the parent layer
     */
    inject: function (parentLayer) {
        if (this.parentLayer && this.parentLayer !== parentLayer) {
            this.remove();
        }
        if (!this.parentLayer) {
            parentLayer.addChild(this);
        }
    },

    /**
     * Inject a layer before a reference layer
     *
     * @param {RenderLayer} parentLayer
     * @param {RenderLayer} referenceLayer
     */
    injectBefore: function (parentLayer, referenceLayer) {
        this.inject(parentLayer);
    },

    /**
     * Add a child to the render layer.
     *
     * @param {RenderLayer} child
     */
    addChild: function (child) {
        child.parentLayer = this;
		// append child to children list
        this.children.push(child);
    },

    /**
     * Remove a layer from it's parent layer.
     */
    remove: function () {
        if (this.parentLayer) {
            this.parentLayer.children.splice(this.parentLayer.children.indexOf(this), 1);
        }
    },

    getChildById: function (childId) {
        for (var i = 0; i < this.children.length; ++i) {
            if (this.children [i] && childId === this.children [i].id) {
                return this.children [i];
            }
        }
        return null;
    },

    /**
     * This function is used to draw custom stuff of the RenderLayer.
     * It will be called *after* the layer rectangle is drawn, and
     * *before* child layers' are drawn.
     */
    drawRenderLayer: function (ctx, translatedX, translatedY) {
        // Do nothing by default
    }
};

module.exports = RenderLayer;