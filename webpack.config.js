/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * The webpack configuration file to serve the examples.
 * @version 1.0
 * @author TCSASSEMBLER
 */
module.exports = {
    cache: true,
    watch: true,
    entry: {
        'webgl': ['./examples/webgl/app.js'],
        'quad': ['./examples/quad/app.js'],
        'performance': ['./examples/performance/app.js'],
        'image-text': ['./examples/image-text/app.js'],
        'image-text-perf': ['./examples/image-text-perf/app.js'],
        'poc': ['./examples/poc/app.js']
    },

    output: {
        filename: '[name].js'
    },

    module: {
        loaders: [
            {test: /\.js$/, loader: 'jsx-loader'},
        ]
    },

    resolve: {
        root: __dirname,
        alias: {
            'react-webgl': '../../src/ReactWebGL.js'
        }
    }
};